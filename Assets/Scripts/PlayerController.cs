﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;
    public bool touchingFloor = true;
    public float jumpForce;
    public int jumps = 0;

    private GravityBody _gravityBody;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        _gravityBody = transform.GetComponent<GravityBody>();
    }
    void Update()
    {
        bool salto = Input.GetKeyDown(KeyCode.Space);

        Vector3 movement = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        transform.position = Vector3.MoveTowards(transform.position, transform.position + movement, speed * Time.deltaTime);

        if (salto && touchingFloor)
        {
            rb.velocity = new Vector3(0f, jumpForce, 0f * Time.deltaTime);
            //rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            rb.AddForce(-_gravityBody.GravityDirection * jumpForce, ForceMode.Impulse);
            touchingFloor = false;
            jumps++;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        touchingFloor = true;
        jumps = 0;
    }

}
